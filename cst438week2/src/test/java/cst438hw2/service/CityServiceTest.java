package cst438hw2.service;
 
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import cst438hw2.domain.*;
 
@SpringBootTest
public class CityServiceTest {

	@MockBean
	private WeatherService weatherService;
	
	@Autowired
	private CityService cityService;
	
	@MockBean
	private CityRepository cityRepository;
	
	@MockBean
	private CountryRepository countryRepository;

	
	@Test
	public void contextLoads() {
	}


	@Test
	public void testCityFound() throws Exception {
		// TODO test with a valid city
		City found = new City(923, "Mixco", "GTM", "Guatemala", 209791);
		List<City> foundList = new ArrayList<City>();
		Country country = new Country("GTM", "Guatemala");
		CityInfo myCity = cityService.getCityInfo("Mixco");
		foundList.add(found);
		given(cityRepository.findByName("Mixco")).willReturn(foundList);
		given(countryRepository.findByCode("GTM")).willReturn(country);
		assertThat(myCity.getCountryCode()).isEqualTo("GTM");
		assertThat(myCity.getCountryName()).isEqualTo("Guatemala");
		assertThat(myCity.getDistrict()).isEqualTo("Mixco");
		assertThat(myCity.getPopulation()).isEqualTo(209791);
	}
	
	@Test 
	public void  testCityNotFound() {
		// TODO test with an unknown city
		City found = new City(923, "Mixco", "GTM", "Guatemala", 209791);
		List<City> foundList = new ArrayList<City>();
		Country country = new Country("GTM", "Guatemala");
		CityInfo myCity = cityService.getCityInfo("Dinotopia");
		foundList.add(found);
		given(cityRepository.findByName("Mixco")).willReturn(foundList);
		given(countryRepository.findByCode("GTM")).willReturn(country);
		assertThat(myCity).isNull();
		
	}
	
	@Test 
	public void  testCityMultiple() {
		// TODO test where there are mutliple cities by that name
		City foundA = new City(2, "Sonora", "USA", "California", 35058);
		City foundB = new City(48, "Sonora", "MEX", "Mexicon", 548679);
		List<City> foundList = new ArrayList<City>();
		Country country = new Country("USA", "United States");
		CityInfo myCity = cityService.getCityInfo("Sonora");
		foundList.add(foundA);
		foundList.add(foundB);
		given(cityRepository.findByName("Sonora")).willReturn(foundList);
		given(countryRepository.findByCode("USA")).willReturn(country);
		assertThat(myCity.getCountryCode()).isEqualTo("USA");
		assertThat(myCity.getCountryName()).isEqualTo("United States");
		assertThat(myCity.getDistrict()).isEqualTo("California");
		assertThat(myCity.getPopulation()).isEqualTo(35058);
		
	}

}
