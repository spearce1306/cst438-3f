package cst438hw2.controller;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import cst438hw2.domain.*;
import cst438hw2.service.CityService;

@Controller
public class CityController {
	
	@Autowired
	private CityService cityService;
	
	@GetMapping("/cities/{city}")
	public String getWeather(@PathVariable("city") String cityName, Model model) {
		CityInfo service = cityService.getCityInfo(cityName);
		if (service == null) {
			return null;
		}
		model.addAttribute("id", service.getId());
		model.addAttribute("name", service.getName());
		model.addAttribute("countryName", service.getCountryName());
		model.addAttribute("countryCode", service.getCountryCode());
		model.addAttribute("district", service.getDistrict());
		model.addAttribute("population", service.getPopulation());
		model.addAttribute("weather", service.getTemp());
		model.addAttribute("time", service.getTime());
		
		return "cityInfo";
	}
	
	@PostMapping("/cities/reservation")
	public String createReservation(@RequestParam("cityName") String cityName, @RequestParam("level") String level, @RequestParam("email") String email, Model model) {
		model.addAttribute("level", level);
		model.addAttribute("cityName", cityName);
		model.addAttribute("email", email);
		cityService.requestReservation(cityName, level, email);
		return "request_reservation";
	}
	
}