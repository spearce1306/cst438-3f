package cst438hw2.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;

import cst438hw2.domain.*;

@Service
public class CityService {
	
	private static final Logger log = LoggerFactory.getLogger(CityService.class);
	
	@Autowired
	private CityRepository cityRepository;
	
	@Autowired
	private CountryRepository countryRepository;
	
	@Autowired
	private WeatherService weatherService;
	
	@Autowired
	private RabbitTemplate rabbitTemplate;
	
	@Autowired
	private FanoutExchange fanout;
	
	public CityInfo getCityInfo(String cityName) {
		log.info("City retrieve: " + cityName);
		List<City> city = cityRepository.findByName(cityName);
		if (city != null) {
			City mainCity = city.get(0);
			Country country = countryRepository.findByCode(mainCity.getCountryCode());
			String countryName = " ";
			if(country == null) {
				countryName = "City not Found";
			} else {
				countryName = country.getName();
			}
			TempAndTime tempTime = weatherService.getTempAndTime(cityName);
			double temp = tempTime.temp;
			String time = tempTime.time;
			CityInfo updatedCity = new CityInfo(mainCity, countryName, temp, time);
			return updatedCity;			
		} else {
			log.info("City retrieve failed. Not found. : " + cityName);
			return null;
		}
	}
	
	public void requestReservation(String cityName, String level, String email) {
		String msg = "{\"cityName\": \"" + cityName + "\" \"level\": \"" + level + "\" \"email\": \"" + email + "\"}";
		System.out.println("Sending message: " + msg);
		rabbitTemplate.convertSendAndReceive(fanout.getName(), "", msg); // "" = routing key none.
	}
	
	
}

