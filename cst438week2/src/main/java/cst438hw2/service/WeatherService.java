package cst438hw2.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import com.fasterxml.jackson.databind.JsonNode;
import cst438hw2.domain.TempAndTime;

@Service
public class WeatherService {
	
	private static final Logger log = LoggerFactory.getLogger(WeatherService.class);
	private RestTemplate restTemplate;
	private String weatherUrl;
	private String apiKey;
	
	
	public WeatherService(
			@Value("${weather.url}") final String weatherUrl, 
			@Value("${weather.apikey}") final String apiKey ) {
		this.restTemplate = new RestTemplate();
		this.weatherUrl = weatherUrl;
		this.apiKey = apiKey; 
	}
	
	public  TempAndTime getTempAndTime(String cityName) {
		ResponseEntity<JsonNode> response = restTemplate.getForEntity(
				weatherUrl + "?q=" + cityName + "&appid=" + apiKey,
				JsonNode.class);
		JsonNode json = response.getBody();
		log.info("Status code from weather server:" + response.getStatusCodeValue());
		double temp = json.get("main").get("temp").asDouble();
		temp = Math.round((temp - 273.15) * 9.0/5.0 + 32.0);
		long time = json.get("dt").asLong();
		int timezone = json.get("timezone").asInt();
		long newTime = time * 1000L;
		DateFormat df = new SimpleDateFormat("hh:mm aa");
		String a[] = TimeZone.getAvailableIDs(timezone*1000);
		if(a.length > 0) {
			df.setTimeZone(TimeZone.getTimeZone(a[0]));
		}
		String formTime = df.format(newTime);
		return new TempAndTime(temp, formTime, timezone);
	}
}
